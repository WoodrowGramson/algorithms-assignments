package utils;

import java.util.Arrays;
import java.util.PriorityQueue;

import utils.WeightedGraph.Edge;

/**
 * Parent class for implementing spanning tree algorithms.
 */
public class Spanning {
	/**
	 * Implements Prim's algorithm
	 */
	public static class Prim {
		private WeightedGraph graph; // The initial graph
		public WeightedGraph tree;   // Contains the resulting tree
		private boolean[] visited; 
		private PriorityQueue<Edge> queue;
		
		public Prim(WeightedGraph graph) {
			this.graph = graph;
			int order = this.graph.order();
			this.tree = new WeightedGraph(order);
			this.visited = new boolean[order];
			Arrays.fill(visited, false);
			this.queue = new PriorityQueue<Edge>();
			
			this.run();
		}
		
		private void run() {
			this.processVertex(1);
			while (!queue.isEmpty()) {
				// TODO: Fix next line to remove the next queue element and store it in nextEdge
				Edge nextEdge = queue.poll();
				// TODO: Check if both vertices in nextEdge are visited, and if they are simply skip this iteration
				if ((visited[nextEdge.v1] == true) && (visited[nextEdge.v2] == true)) {
					continue;
					}
				// TODO: This is now a valid edge. Add it to the tree.
				tree.addEdge(nextEdge);
				// And now we proceed to process the edge.
				int newVertex = findUnvisitedVertex(nextEdge);
				this.processVertex(newVertex);
			}
		}
		
		private int findUnvisitedVertex(Edge edge) {
			// TODO: You can assume one of the vertices in the edge is visited and the other is not. 
			// This method must return the one that is not.
			if (visited[edge.v1] == true) {
				return edge.v2;
			}
			return edge.v1;
		}
		
		private void processVertex(int i) {
			//	 TODO: Need to process a new vertex
			// 1. Mark the vertex as visited.
			visited[i] = true;
			// 2. Loop through the "outgoing edges".
			for(Edge myEdge : this.graph.outgoingEdges(i)) {
				// 3. For each such edge, find the "other vertex" than i. Method in Edge class can help.
				int other = myEdge.getOtherVertex(i);
				if (visited[other] == false) {
					// 4. If that other vertex is not yet visited, add the edge to the queue.
					queue.add(graph.getEdge(i, other));
				}
			}
			
			// 4. If that other vertex is not yet visited, add the edge to the queue.
		}
	}

	/**
	 * Implements Kruskal's algorithm
	 */
	public static class Kruskal {
		private WeightedGraph graph; // The initial graph
		public WeightedGraph tree;   // Contains the resulting tree
		private int[] unionFind;     // Holds the union-find structure of the various vertex components 
		private PriorityQueue<Edge> queue;  // Holds a priority queue of the edges by their weight
		
		public Kruskal(WeightedGraph graph) {
			this.graph = graph;
			int order = this.graph.order();
			this.tree = new WeightedGraph(order);
			this.unionFind = new int[order];
			this.queue = new PriorityQueue<Edge>();
			
			this.run();
		}
		
		private void run() {
			this.initializeUnionFind();
			this.addAllEdgesToQueue();
			
			while (!queue.isEmpty()) {
				// TODO: 
				// 1. Remove the next edge from the queue and store it in a variable
				Edge nextEdge = queue.poll();
				// 2. Use areConnected to check if the two vertices of that edge are already connected
				if (!areConnected(nextEdge.v1, nextEdge.v2)) {
					tree.addEdge(nextEdge);
					union(nextEdge.v1, nextEdge.v2);
				}
				// 3. If they are not, then add the edge to the tree
				// 4. And use the union method to connect the two vertices in the union-find
			}
		}
			
		private void addAllEdgesToQueue() {
			// TODO: Loop over all the edges (loop over the vertices, then over the outgoing edges of each vertex)
			// and add each edge to the queue. Make sure to only add each edge once, even though your loop encounters
			// each edge twice. One way to do it is to only add an edge if its v1 vertex matches the vertex you are currently looping in.
			for (int i=0; i<this.graph.order(); i++){
				for (Edge j: this.graph.outgoingEdges(i)){
					if (j.v1 == i) {
						queue.add(j);
					}
				}
			}
		}

		private void initializeUnionFind() {
			// TODO: Go through all vertices, and make them point to themselves (they are not connected to anything yet).
			for(int i =0; i<this.graph.order(); i++) {
				this.unionFind[i] = i;
			}
			
		}
		
		private boolean areConnected(int i, int j) {
			int ri = this.findRoot(i);
			int rj = this.findRoot(j);
			
			return ri == rj;
		}
		
		private void union(int i, int j) {
			// TODO: Find the roots corresponding to i and j
			// Then link them by making one point to the other.
			int ri = this.findRoot(i);
			int rj = this.findRoot(j);
			
			unionFind[ri] = rj;
		}
		
		private int findRoot(int i) {
			// TODO: 
			// 1. Traverse the unionFind pointers upwards to find the root. Use a local variable to track your location
			int j = i;
			while (unionFind[j] != j) {
				j = unionFind[j];
				
			}
			// 2. Change the pointer of vertex i to point straight to its root.
			unionFind[i] = j;
			// 3. Return the root.
				
			return j;
		}
	}
	
	public static void main(String[] args) {
		WeightedGraph g = new WeightedGraph(6);
		g.addEdge(0, 1, 3);  // a-b-3
		g.addEdge(0, 4, 6);  // a-e-6
		g.addEdge(0, 5, 5);  // a-f-5
		g.addEdge(1, 2, 1);  // b-c-1
		g.addEdge(1, 5, 4);  // b-f-4
		g.addEdge(2, 3, 6);  // c-d-6
		g.addEdge(2, 5, 4);  // c-f-4
		g.addEdge(3, 4, 8);  // d-e-8
		g.addEdge(4, 5, 2);  // e-f-2
		System.out.println("############################################");
		System.out.println("##########     PRIM's Output     ###########");
		System.out.println("############################################");
		Prim prim = new Prim(g);
		WeightedGraph tree = prim.tree;
		for (int i = 0; i < tree.order(); i += 1) {
			for (Edge edge : tree.outgoingEdges(i)) {
				if (i == edge.v1) {
					System.out.println(edge);
				}
			}
		}
		System.out.println("############################################");
		System.out.println("##########   KRUSKAL's Output   ############");
		System.out.println("############################################");
		Kruskal kruskal = new Kruskal(g);
		tree = kruskal.tree;
		for (int i = 0; i < tree.order(); i += 1) {
			for (Edge edge : tree.outgoingEdges(i)) {
				if (i == edge.v1) {
					System.out.println(edge);
				}
			}
		}
		
		// Add your own printouts here
	}
}

