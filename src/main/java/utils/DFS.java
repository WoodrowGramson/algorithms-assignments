package utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;

/**
 * Class implementing a DFS search through a graph. It expects to be
 * provided with a "visitor" object which performs a suitable task
 * on each visited vertex.
 * @author Charilaos Skiadas
 *
 */
public class DFS {
	/**
	 * Carries out a simple DFS run and just prints results.
	 */
	private static class SimpleDFS {
		private SimpleGraph graph;         // The graph to be traversed
		private int order;                 // The graph order, stored separately
		public boolean[] visited;          // Keeps track of visited vertices
		public int[] parent;               // The "parent" of a vertex in the DFS tree 
		public List<Integer> pushOrder;  // The vertices in the order in which they are placed in the stack
		public List<Integer> popOrder;   // The vertices in the order in which they become dead-ends (get popped from the stack)
		private Stack<Integer> stack;     // Maintains the vertices in stack form 

		public SimpleDFS(SimpleGraph graph) {
			this.graph = graph;
			this.order = graph.order();
		}
		
		public void run() {
			visited = new boolean[order];
			parent = new int[order];
			Arrays.fill(visited, false);
			Arrays.fill(parent, -1);
			stack = new Stack<Integer>();
			pushOrder = new ArrayList<Integer>() ;
			popOrder = new ArrayList<Integer>() ;

			// Now we start the run
			for (int startVertex = 0; startVertex < this.order; startVertex += 1) {
				if (!visited[startVertex]) {
					System.out.format("NEW START VERTEX: %d\n", startVertex);
					// We start a dfs from this root vertex
					this.dfs(startVertex);
				}
			}
      		// We arrived at the end of the array of vertices without new start point
			System.out.println("DONE!");
			this.printOrders();
		}

		/**
		 *  Carries out a dfs search starting from the given vertex
		 * @param currentVertex    the vertex to descend from.
		 */
		public void dfs(int currentVertex) {
			System.out.format("Visiting vertex: %d\n", currentVertex);
			stack.push(currentVertex);
			pushOrder.add(currentVertex);
			visited[currentVertex] = true;
			
			
			// Now we must recurse over the neighboring vertices
			for (Integer nextVertex : this.graph.outgoingEdges(currentVertex)) {
				if (this.parent[currentVertex] != nextVertex) {
					// This is an actual new edge and not the one we just traversed.
					if (visited[nextVertex]) {
						// We encountered a back edge
						System.out.format("Back edge: %d - %d\n", nextVertex, currentVertex);
					} else {
						// We encountered a tree edge 
						System.out.format("Tree edge: %d - %d\n", currentVertex, nextVertex);
						parent[currentVertex] += 1;
						dfs(nextVertex);
					}
				}
			}
			stack.pop();
			popOrder.add(currentVertex); 
			
			
			System.out.format("Done visiting vertex (dead-end): %d\n", currentVertex);
		}

		/**
		 * Print the push and pop orders of the vertices//
		 */
		private void printOrders() {
			System.out.print("PUSH ORDER: ");
			DFS.printList(Arrays.asList(pushOrder));
			System.out.println();
			System.out.print("POP ORDER: ");
			DFS.printList(Arrays.asList(popOrder));
			System.out.println();
		}
	}
		

	
	
	/**
	 * ComponentsDFS
	 * 
	 * Performs DFS search and collects the connected components of the graph
	 */
	private static class ComponentsDFS {
		private SimpleGraph graph;      // The graph to be traversed
		private int order;              // The graph order, stored separately
		public boolean[] visited;       // Keeps track of visited vertices
		public int[] parent;            // The "parent" of a vertex in the DFS tree 
		public List<Set<Integer>> components;    // The components to be returned 

		public ComponentsDFS(SimpleGraph graph) {
			this.graph = graph;// 
			this.order = graph.order();
		}
		
		public List<Set<Integer>> run() {
			visited = new boolean[order];
			parent = new int[order];
			Arrays.fill(visited, false);
			Arrays.fill(parent, -1);
			components = new ArrayList<Set<Integer>>();
			
			
			
			// Now we start the run
			for (int startVertex = 0; startVertex < this.order; startVertex += 1) {
				if (!visited[startVertex]) {
					Set<Integer> b = new HashSet<Integer>();
					components.add(b);
					
					
					// We start a dfs from this root vertex
					this.dfs(startVertex);
				}
			}
      		// We arrived at the end of the array of vertices without new start point
			this.printComponents();
			
			return components;
		}

		/**
		 *  Carries out a dfs search starting from the given vertex
		 * @param currentVertex    the vertex to descend from.
		 */
		public void dfs(int currentVertex) {
			visited[currentVertex]= true;
			components.get(components.size()-1).add(currentVertex);
			
			
			// Now we must recurse over the neighboring vertices
			for (Integer nextVertex : this.graph.outgoingEdges(currentVertex)) {
				if (this.parent[currentVertex] != nextVertex) {
					// This is an actual new edge and not the one we just traversed.
					if ( ! visited[nextVertex]) {
						// We encountered a tree edge. This reaches a new vertex that is part of the same component. 
						parent[currentVertex] += 1;
						dfs(nextVertex);
						
					}
					// No else clause needed, as we do not need to do anything on back edges.
				}
			}
			// Nothing else to do
		}

		/**
		 * Print the various connected components
		 */
		private void printComponents() {
			System.out.format("Found %d components:\n", this.components.size());
			for (int i = 0; i < this.components.size(); i += 1) {
				Set<Integer> component = this.components.get(i);
				System.out.format("%d : ", i + 1);
				DFS.printList(component);
				System.out.println();
			}
		}
	}
	

	/**
	 * CycleDFS
	 * 
	 * Performs DFS search and to find a cycle.
	 */
	private static class CycleDFS {
		private SimpleGraph graph;      // The graph to be traversed
		private int order;              // The graph order, stored separately
		public boolean[] visited;       // Keeps track of visited vertices
		public int[] parent;            // The "parent" of a vertex in the DFS tree 
		public List<Integer> cycle;   // The final cycle, if there is one. Empty if there is no cycle
		
		public CycleDFS(SimpleGraph graph) {
			this.graph = graph;
			this.order = graph.order();
		}
		
		public List<Integer> run() {
			visited = new boolean[order];
			parent = new int[order];
			Arrays.fill(visited, false);
			Arrays.fill(parent, -1);
			cycle = new ArrayList<Integer>() ;
			
			// Now we start the run
			for (int startVertex = 0; startVertex < this.order; startVertex += 1) {
				if (!visited[startVertex]) {
					// Nothing special to do for new vertices
					// We start a dfs from this root vertex
					this.dfs(startVertex);
				}
			}
      		// We arrived at the end of the array of vertices without new start point
			this.printCycle();
			
			return cycle;
		}

		/**
		 *  Carries out a dfs search starting from the given vertex
		 * @param currentVertex    the vertex to descend from.
		 */
		public void dfs(int currentVertex) {
			visited[currentVertex] = true;
			// We must recurse over the neighboring vertices
			for (Integer nextVertex : this.graph.outgoingEdges(currentVertex)) {
				
				if (!cycle.isEmpty()) {
					return;
				}
				if (this.parent[currentVertex] != nextVertex) {
					// This is an actual new edge and not the one we just traversed.
					if ( ! visited[nextVertex]) {
						// We encountered a tree edge. 
						
						parent[currentVertex] += 1;
						dfs(nextVertex);
					} else {
						// A back edge encountered. Means that nextVertex is an ancestor of currentVertex. 
						// We therefore have a cycle consisting of the ancestor path via the parent array, from currentVertex up to next Vertex
						// followed by this back edge we just discovered. We must record this cycle.
						int c =currentVertex;
						while (parent[currentVertex] != nextVertex) {
							cycle.add(currentVertex);
							nextVertex= parent[currentVertex];
						}
						cycle.add(nextVertex);
						currentVertex = c;
						
						
						// Once this cycle is recorded, there is no need to continue traversing the graph. We start returning.
						return;
					}
				}	
			}
			// Nothing else to do
		}

		/**
		 * Print the cycle if one was found, or a message otherwise
		 */
		private void printCycle() {
			if (this.cycle.isEmpty()) {
				System.out.println("The graph is acyclic");
			} else {
				System.out.print("Found cycle: ");
				DFS.printList(this.cycle);
				System.out.println();
			}
		}
	}
	


	/**
	 * BipartiteDFS
	 * 
	 * Performs DFS search to attempt to determine whether the graph is bipartite.
	 */
	private static class BipartiteDFS {
		private SimpleGraph graph;   // The graph to be traversed
		private int order;           // The graph order, stored separately
		public boolean[] visited;    // Keeps track of visited vertices
		public int[] parent;            // The "parent" of a vertex in the DFS tree 
		public int[] colors;         // One of two "colors" used for the two bipartite sets of vertices 
		public boolean failed;       // True if the attempt to use two colors failed
		
		public BipartiteDFS(SimpleGraph graph) {
			this.graph = graph;
			this.order = graph.order();
		}
		
		public void run() {
			visited = new boolean[order];
			parent = new int[order];
			colors = new int[order];
			Arrays.fill(parent, -1);
			Arrays.fill(colors, 0) ;
			Arrays.fill(visited, false);
			failed = false;
			
			
			// Now we start the run
			for (int startVertex = 0; startVertex < this.order; startVertex += 1) {
				if (!visited[startVertex]) {
					
					colors[startVertex] = 1;

					// We start a dfs from this root vertex
					this.dfs(startVertex);
				}
			}
      		// We arrived at the end of the array of vertices without new start point
			this.printSets();
		}

		/**
		 * Carries out a dfs search starting from the given vertex
		 * @param currentVertex    the vertex to descend from.
		 */
		public void dfs(int currentVertex) {
			visited[currentVertex] = true;
			
			// We must recurse over the neighboring vertices
			for (Integer nextVertex : this.graph.outgoingEdges(currentVertex)) {
				if (failed == true) {
					return;
				}
				if (this.parent[currentVertex] != nextVertex) {
					// This is an actual new edge and not the one we just traversed.
					if ( ! visited[nextVertex]) {
						// We encountered a tree edge. 
						parent[currentVertex] += 1;
						colors [nextVertex] = (3 - colors [currentVertex]) ;
						dfs(nextVertex);

					} else {
						// A back edge encountered. This may be a conflict if the colors are not compatible. 
						if (colors[currentVertex] == colors[nextVertex]) {
							failed = true;
						}

					}
				}
			}
			// Nothing else to do
		}

		/**
		 * Print the two sets that were found, or an error message
		 */
		private void printSets() {
			if (this.failed) {
				System.out.println("The graph is NOT bipartite");
			} else {
				System.out.println("The graph is bipartite! The parts are: ");
				List<Integer> color1 = new ArrayList<Integer>();
				List<Integer> color2 = new ArrayList<Integer>();
				for (int i = 0; i < this.colors.length; i += 1) {
					if (this.colors[i] == 1) {
						color1.add(i);
					} else {
						color2.add(i);
					}
				}
				System.out.print("Part 1: ");
				DFS.printList(color1);
				System.out.println();
				System.out.print("Part 2: ");
				DFS.printList(color2);
				System.out.println();
			}
		}
	}
	


	
	// Helper method for printing a list of items separated by commas
	// You do not need to change anything here
	private static <T> void printList(Iterable<T> list) {
		Iterator<T> iterator = list.iterator();
		if (! iterator.hasNext()) {
			return;
		}
		System.out.print(iterator.next());
		while (iterator.hasNext()) {
			System.out.print(", ");
			System.out.print(iterator.next());
		}
	}
	
	
	public static void main(String[] args) {
		// This is the graph from figure 3.10, with the numbers 0, ..., 9 corresponding to the letters a, ..., j
		SimpleGraph g1 = new SimpleGraph(10);
		g1.addBidirectionalEdge(0, 2);      // a-c
		g1.addBidirectionalEdge(0, 3);      // a-d
		g1.addBidirectionalEdge(0, 4);      // a-e
		g1.addBidirectionalEdge(2, 3);      // c-d
		g1.addBidirectionalEdge(1, 4);      // b-e   
		g1.addBidirectionalEdge(1, 5);      // b-f
		g1.addBidirectionalEdge(2, 5);      // c-f   
		g1.addBidirectionalEdge(4, 5);      // e-f
		g1.addBidirectionalEdge(6, 7);      // g-h
		g1.addBidirectionalEdge(7, 8);      // h-i   
		g1.addBidirectionalEdge(8, 9);      // i-j   
		g1.addBidirectionalEdge(6, 9);      // g-j   

		// Simple DFS
		SimpleDFS simpleDFS = new SimpleDFS(g1);
		simpleDFS.run();    
		// Push order should be: a, c, d, f, b, e, g, h, i, j
		// Pop order should be: d, e, b, f, c, a, j, i, h, g
		// Back edges: d-a, e-f, e-a, j-g
		
		// Components DFS
		ComponentsDFS componentsDFS = new ComponentsDFS(g1);
		componentsDFS.run();
		// Components should be a, b, c, d, e, f   and g, h, i, j
		
		CycleDFS cycleDFS = new CycleDFS(g1);
		cycleDFS.run();
		// A cycle based on one of the back edges should have been printed out
		
		BipartiteDFS bipartiteDFS = new BipartiteDFS(g1);
		bipartiteDFS.run();
		// This should have been a failed search.
		
		// A small square graph to show bipartite in action. Should find two sets, 0, 2 and 1, 3
		SimpleGraph g2 = new SimpleGraph(4);
		g2.addBidirectionalEdge(0, 1); 
		g2.addBidirectionalEdge(1, 2); 
		g2.addBidirectionalEdge(2, 3); 
		g2.addBidirectionalEdge(3, 0);
		BipartiteDFS bipartiteDFS2 = new BipartiteDFS(g2);
		bipartiteDFS2.run();
	}
}
