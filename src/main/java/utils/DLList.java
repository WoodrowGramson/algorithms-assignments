package utils;

import java.util.AbstractSequentialList;
import java.util.Iterator;
import java.util.Deque;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Simple double-linked list implementation
 * 
 * A simple implementation using a sentinel node to mark the
 * start and end of the list. The list consists of nodes, 
 * where each {@link Node} has an item, and prev/next references pointing
 * to the nodes before and after it in the list.
 * 
 * The list starts with a "sentinel" node that marks the start and
 * end of the loop of nodes that form the list. Using a sentinel
 * node allows you to implement most methods without having to handle
 * the empty case specially.
 * @author Graham Wood
 * 
 * @param <E>  the class of elements that the list contains
 * @see java.util.Deque
 */
public class DLList<E> extends AbstractSequentialList<E> implements Deque<E> {
	private Node sentinel;
	
	// A private class used to implement list iteration
	private class DLListIterator implements ListIterator<E> {
		private DLList<E> list;       // the list
		private Node curr;            // The current node (to be returned by next).
		private int index;            // The current node's "index"
		private boolean afterNext;    // Internal flag used to determine if remove or set are valid. 
		
		public DLListIterator(DLList<E> list, int start) {
			this.list = list;
			this.index = start;
			this.afterNext = false;
			// We must manually find the start node
			// to avoid the exceptions thrown by getNodeAtIndex
			// when index == list.size()
			this.curr = list.sentinel;
			if (start < 0) {
				throw new IndexOutOfBoundsException();
			}
			while (true) {
				this.curr = this.curr.next;
				if (start == 0) { break; } // found start node
				if (this.curr == list.sentinel) {
					// We reached the end of the list without
					// reaching the start index. It was too large.
					throw new IndexOutOfBoundsException();
				}
				start -= 1;
			}
			
		}

		// Adds the element at the current cursor, advancing the cursor.
		@Override
		public void add(E el) {
			this.curr.addBefore(el);
			this.index += 1;
			this.afterNext = false;
		}

		@Override
		public boolean hasNext() {
			return this.curr != this.list.sentinel;
		}

		@Override
		public boolean hasPrevious() {
			return this.curr.prev != this.list.sentinel;
		}

		@Override
		public E next() {
			if (this.curr == this.list.sentinel) {
				throw new NoSuchElementException();
			}
			
			E item = this.curr.item;			
			this.curr = this.curr.next;
			this.index += 1;
			this.afterNext = true;
			
			return item;
		}

		@Override
		public int nextIndex() {
			return this.index;
		}

		@Override
		public E previous() {
			if (this.curr.prev == this.list.sentinel) {
				throw new NoSuchElementException();
			}
			
			this.curr = this.curr.prev;
			this.index -= 1;
			this.afterNext = true;
			
			return this.curr.item;
		}

		@Override
		public int previousIndex() {
			return this.index - 1;
		}

		@Override
		public void remove() {
			if (!this.afterNext) {
				throw new IllegalStateException();
			}
			this.afterNext = false;

			this.curr.prev.removeSelf();
		}

		@Override
		public void set(E o) {
			if (!this.afterNext) {
				throw new IllegalStateException();
			}
			this.curr.prev.item = o;
		}
		
	}

	// A private class used to implement reverse iteration through the list
	private class DLListReverseIterator implements Iterator<E> {
		private DLList<E> list;       // the list
		private Node curr;            // The current node (to be returned by next).
		private boolean afterNext;    // Internal flag used to determine if remove or set are valid. 
		
		public DLListReverseIterator(DLList<E> list) {
			this.list = list;
			this.curr = list.sentinel.prev;
			this.afterNext = false;
		}

		@Override
		public boolean hasNext() {
			return this.curr != this.list.sentinel;
		}

		@Override
		public E next() {
			if (this.curr == this.list.sentinel) {
				throw new NoSuchElementException();
			}
			
			E item = this.curr.item;			
			this.curr = this.curr.prev;
			this.afterNext = true;
			
			return item;
		}

		@Override
		public void remove() {
			if (!this.afterNext) {
				throw new IllegalStateException();
			}
			this.afterNext = false;

			this.curr.next.removeSelf();
		}
	}

	// A private class used to implement the linked list nodes
	private class Node {
		public E item;
		public Node prev;
		public Node next;
		
		/**
		 * Create an isolated node 
		 * @param item    the item contained in the node
		 */
		public Node(E item) {
			this.item = item;
			this.prev = null;
			this.next = null;
		}
		/**
		 * Create a node with specified predecessor and successor.
		 * 
		 * NOTE: This will not adjust the references at those nearby nodes
		 * to point to the new node. It will be the task of the caller
		 * to make such arrangements.
		 * @param prev   the node's predecessor
		 * @param item   the item contained in the node
		 * @param next   the node's successor
		 */
		public Node(Node prev, E item, Node next) {
			this.item = item;
			this.prev = prev;
			this.next = next;
		}
		
		/**
		 * Create and add a new node after this node, using the provided element.
		 * @param item   the element to add to the newly created node
		 * @return       the newly created node
		 */
		public Node addAfter(E item) {
			// Add a new node with the correct predecessor and successor
			Node newNode = new Node(this, item, this.next);
			// Adjust the old references
			this.next.prev = newNode;
			this.next = newNode;
			
			return newNode;
		}
		/**
		 * Create and add a new node before this node, using the provided element.
		 * @param item   the element to add to the newly created node
		 * @return       the newly created node
		 */
		public Node addBefore(E item) {
			// Add a new node with the correct predecessor and successor
			Node newNode = new Node(this.prev, item, this);
			// Adjust the old references
			this.prev.next = newNode;
			this.prev = newNode;
			
			return newNode;
		}
		/**
		 * Remove this node from its list, adjusting its neighbors
		 * @return the removed node
		 */
		public Node removeSelf() {
			// Adjust the references
			this.next.prev = this.prev;
			this.prev.next = this.next;
			
			return this;
		}
	}
	/**
	 * Create a new empty double-linked list. 
	 * 
	 * The list starts with a sentinel node, which should not count towards the list elements.
	 * The sentinel node marks the beginning and end of the list (resulting in a circular list if the sentinel is included.
	 */
	public DLList() {
		this.sentinel = new Node(null);
		this.sentinel.next = this.sentinel;
		this.sentinel.prev = this.sentinel;
	}
	/*
	 * Return the node at a specified index. Throws an IndexOutOfBoundsException if the index is out of bounds.
	 */
	private Node getNodeAtIndex(int index) {
		// curr represents the current node.
		// We will advance that node along with decrementing the
		// index, until the index is zero or we run out of nodes
		Node curr = this.sentinel.next;
		// As long as the index is not negative and we have not 
		// reached the end of the list
		while (index >= 0 && curr != this.sentinel) {
			// First check if the index is zero, and if 
			// it is then return the curr node
			if (index == 0) {
				return curr ;
			}
			// Then set curr to the next node 
			// and decrement the index by one
			curr = curr.next ;
			index = index - 1 ;
		}		
		// Here we ran out of nodes so we throw an exception
		throw new IndexOutOfBoundsException();
	}

	/**
	 * Return an iterator that starts at a specified index location.
	 * @see java.util.AbstractSequentialList#listIterator(int)
	 */
	@Override
	public ListIterator<E> listIterator(int start) {
		return new DLListIterator(this, start);
    }

	@Override
	public boolean isEmpty() {
		return this.sentinel.next == this.sentinel;
	}
	
	/**
	 * @see java.util.Collection#size()
	 */
	public int size() {
		int count = 0;
		// This for loop will go through each element in the 
		// list in order from front to back
		for (E el : this) {
			// You simply have to increment the count.
			// So we won't really use the element `el`
			// But in other situations maybe we would have
			count++ ;
			
		} 

		return count;
	}
	/////////////////////////////////////////// 
	//  METHODS IMPLEMENTING DEQUE INTERFACE //
	///////////////////////////////////////////
	@Override
	public void addFirst(E el) {
		this.sentinel.addAfter(el) ;
		
	}
	@Override
	public void addLast(E el) {
		this.sentinel.addBefore(el) ;
		
	}
	@Override
	public E removeFirst() {
		if (this.sentinel.next == this.sentinel) {
			throw new NoSuchElementException();
		}
		else {
			return this.sentinel.next.removeSelf().item ;
		}
	}
	@Override
	public E removeLast() {
		if (this.sentinel.prev == this.sentinel) {
			throw new NoSuchElementException();
		}
		else {
			return this.sentinel.prev.removeSelf().item ;
		}
	}
	@Override
	public E getFirst() {
		if (this.sentinel.next == this.sentinel) {
			throw new NoSuchElementException();
		}
		else {
			return (this.sentinel.next.item) ;
		}
	}
	@Override
	public E getLast() {
		if (this.sentinel.prev == this.sentinel) {
			throw new NoSuchElementException();
		}
		else {
			return (this.sentinel.prev.item) ;
		}
	}
	@Override
	public E peekFirst() {
		if (isEmpty()) {
			return null ;
		}
		else {
			return (this.sentinel.next.item) ;
		}
	}
	@Override
	public E peekLast() {
		if (isEmpty()) {
			return null ;
		}
		else {
			return (this.sentinel.prev.item) ;
		}
	}
	@Override
	public E pollFirst() {
		if (this.sentinel.next == this.sentinel) {
			return null ;
		}
		else {
			return this.sentinel.next.removeSelf().item ;
		}
	}
	@Override
	public E pollLast() {
		if (this.sentinel.prev == this.sentinel) {
			return null ;
		}
		else {
			return this.sentinel.prev.removeSelf().item ;
		}
	}
	@Override
	public boolean removeFirstOccurrence(Object o) {
		Iterator<E> it = this.listIterator();
		while (it.hasNext()) {
			E item = it.next();
			boolean isMatch = o == null ? item == null : o.equals(item);

			if (isMatch) {
				it.remove();
				
				return true;
			}
		}

		return false;
	}
	@Override
	public boolean removeLastOccurrence(Object o) {
		Iterator<E> it = this.descendingIterator();
		while (it.hasNext()) {
			E item = it.next();
			boolean isMatch = o == null ? item == null : o.equals(item);

			if (isMatch) {
				it.remove();
				
				return true;
			}
		}

		return false;
	}
	
	@Override
	public Iterator<E> descendingIterator() {
		return new DLListReverseIterator(this);
	}
	@Override
	public boolean offerFirst(E el) {
		this.addFirst(el);
		
		return true;
	}
	@Override
	public boolean offerLast(E el) {
		this.addLast(el);

		return true;
	}

	// QUEUE METHODS IMPLEMENTED VIA DEQUEUE METHODS
	@Override
	public boolean offer(E el) {
		return this.offerLast(el);
	}
	@Override
	public E remove() {
		return this.removeFirst();
	}
	@Override
	public E poll() {
		return this.pollFirst();
	}
	@Override
	public E element() {
		return this.getFirst();
	}
	@Override
	public E peek() {
		return this.getFirst();
	}
	// STACK METHODS IMPLEMENTED VIA DEQUEUE METHODS
	@Override
	public void push(E el) {
		this.addFirst(el);
	}	
	@Override
	public E pop() {
		return this.removeFirst();
	}
}