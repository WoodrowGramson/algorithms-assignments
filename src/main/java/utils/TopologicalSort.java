package utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Class implementing topological sort via source-removal
 * @author Charilaos Skiadas
 *
 */
public class TopologicalSort {
		private SimpleGraph graph;         // The graph to be traversed
		private int order;                 // The graph order, stored separately
		
		public TopologicalSort(SimpleGraph graph) {
			this.graph = graph;
			this.order = graph.order();
		}
		
		/*
		 * @throws Exception
		 */
		public List<Integer> run() throws Exception {
			List<Integer> resultList;    // Contains the vertices in topological order
			Queue<Integer> processQueue; // Contains the vertices that we can use as sources
			int[] edgeCount;             // Keeps track number of number of incoming edges for each vertex
			
			resultList = new ArrayList<Integer>();
			processQueue = new LinkedList<Integer>();
			edgeCount = new int[order];
	
			
			// TODO: Loop over each vertex i in the graph, and over each edge from i to another
			// vertex j. For each such edge, increment the edgeCount for the target vertex j.
			for (int i=0; i<this.order; i++){
				for (int j: graph.outgoingEdges(i)){
					edgeCount[j] +=1;
				}
			}

			
			// TODO: Loop over the edgeCount array and identify any vertices that have edgeCount 0
			// These are possible source vertices. Add them all to processQueue.
			for (int i=0; i<this.order; i++){
				if (edgeCount[i] == 0){
					processQueue.add(i);
				}
			}

			
			
			// TODO: If the processQueue is empty at this point, throw an Exception as
			// the graph is not acyclic
			if (processQueue.isEmpty()){
				throw new Exception("This graph is not acyclic.");
			}
			
			// TODO: While the processQueue is not empty:
			// 1. Take a vertex out of the queue. It is the next sourceVertex;
			// 2. Put the source vertex in the resultList
			// 3. For each outgoing edge from the vertex, look at the target vertex value in 
			//          edgeCounts. If that count is not > 0 then throw an Exception and look back
			//          at your code. This should not be happening and signals a code error.
			// 4. Decrease the edgeCount of the target vertex by 1 (we just removed that edge)
			// 5. If the edgeCount for this target vertex is now 0, add the target vertex to the queue. 
			//    It is now a source vertex and should be queued for processing.
			while (!processQueue.isEmpty()){
				int sourceVertex = processQueue.poll();
				resultList.add(sourceVertex);
				for (int i: this.graph.outgoingEdges(sourceVertex)) {
					if (edgeCount[i] <= 0){
						throw new Exception("Logic Error");
					}
					edgeCount[i] -= 1;
					if (edgeCount[i] == 0){
						processQueue.add(i);
					}
				}
			}
			
			// We're done!
			return resultList;
		}

	
		
	public static void main(String[] args) throws Exception {
		// This is graph 4.2.1a
		SimpleGraph g1 = new SimpleGraph(7);
		g1.addEdge(0, 1);      // a -> b
		g1.addEdge(0, 2);      // a -> c
		g1.addEdge(1, 4);      // b -> e
		g1.addEdge(1, 6);      // b -> g
		g1.addEdge(2, 5);      // c -> f
		g1.addEdge(3, 0);      // d -> a
		g1.addEdge(3, 1);      // d -> b
		g1.addEdge(3, 2);      // d -> c
		g1.addEdge(3, 5);      // d -> f
		g1.addEdge(3, 6);      // d -> g
		g1.addEdge(6, 4);      // g -> e
		g1.addEdge(6, 5);      // g -> f
		
		// Simple DFS
		TopologicalSort topoSort = new TopologicalSort(g1);
		List<Integer> result = topoSort.run();
		for (Integer i : result) {
			System.out.format(" %c", (char)(i + 97));
		}
		System.out.println();
		
	}
}
