package utils;

import java.util.Random;

public class CompArray {
	private CompElement[] elements; // The element array
	private static int writes = 0;         // the number of array write operations
	private static int comparisons = 0;  // Counts the number of comparisons performed

	/**
	 * Create an empty array of elements
	 * @param size  the size of the array
	 */
	public CompArray(int size) {
		elements = new CompElement[size];
	}

	/**
	 * Generate an array of randomly generated values
	 * @param size             the size of the resulting array
	 * @param maxValue         the largest integer value for the randomly generated elements
	 * @return            An array with size many values, whose elements are randomly generated numbers between 0 and maxValue.
	 */
	public static CompArray makeRandom(int size, int maxValue) {
		CompArray array = new CompArray(size);
		Random random = new Random();
		for (int i = 0; i < size; i += 1) {
			array.elements[i] = new CompElement(random.nextInt(maxValue));
		}
		
		return array;
	}

	public static void resetCounts() {
		comparisons = 0;
		writes = 0;
	}

	public static class CompElement implements Comparable<CompElement> {
		private static Random rand = new Random();
		private static int maxValue = 100;
		private int key;

		public CompElement() {
			this.key = rand.nextInt(maxValue);
		}

		public CompElement(int key) {
			this.key = key;
		}

		public static void setMaxValue(int newMax) {
			maxValue = newMax;
		}

		public int compareTo(CompElement o) {
			comparisons += 1;
			return this.key - o.key;
		}

		public String toString() {
			return Integer.toString(this.key);
		}

		public boolean greaterThan(CompElement o) {
			return this.compareTo(o) > 0;
		}

		public boolean lessThan(CompElement o) {
			return this.compareTo(o) < 0;
		}

		public boolean greaterThanOrEqualTo(CompElement o) {
			return this.compareTo(o) >= 0;
		}

		public boolean lessThanOrEqualTo(CompElement o) {
			return this.compareTo(o) <= 0;
		}
	}


	//  CompArray PUBLIC METHODS

	/**
	 * A.get(i) is equivalent to A[i] in a normal array.
	 * @param i     the index
	 * @return The element stored at entry i
	 */
	public CompElement get(int i) {
		if (i < 0 || i >= elements.length) {
			throw new IndexOutOfBoundsException("i");
		}

		return elements[i];
	}

	/**
	 * A.put(i, v) is equivalent to A[i] = v in a normal array
	 * @param i     the index
	 * @param v     the value to store
	 */
	public void put(int i, CompElement v) {
		if (i < 0 || i >= elements.length) {
			throw new IndexOutOfBoundsException("i");
		}

		writes += 1;
		elements[i] = v;
	}

	/**
	 * A.swap(i, j) swaps the values stored in the entries i, j.
	 * @param i
	 * @param j
	 */
	public void swap(int i, int j) {
		if (i < 0 || i >= elements.length) {
			throw new IndexOutOfBoundsException("i");
		}
		if (j < 0 || j >= elements.length) {
			throw new IndexOutOfBoundsException("j");
		}

		CompElement temp = this.get(i);
		this.put(i, this.get(j));
		this.put(j, temp);
	}

	/**
	 * @return The number of array writes performed.
	 */
	public int getNumWrites() {
		return writes;
	}

	public int getNumComparisons() {
		return comparisons;
	}
	
	/**
	 * @return   the array size
	 */
	public int size() {
		return this.elements.length;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i < elements.length; i += 1) {
			if (i > 0) {
				sBuilder.append(", ");
			}
			sBuilder.append(elements[i].toString());
		}
		
		return sBuilder.toString();
	}
	
	/**
	 * @see java.lang.Object#clone()
	 */
	public CompArray clone() {
		int size = this.size();
		CompArray newArray = new CompArray(size);
		
		for (int i = 0; i < size; i += 1) {
			newArray.put(i, this.get(i));
		}
		
		return newArray;
	}
	
}
