package utils;

import java.util.Arrays;

import utils.CompArray.CompElement;

public class Sort {
	public static void selectionSort(CompArray array) {
		int n = array.size();
		// TODO: Implement selectionSort
		for (int i = 0; i <= n-2; i++) {
			int min = i;
			for (int j = i+1; j <= n-1; j++) {
				if (array.get(j).lessThan(array.get(min))) {
					min = j;
			array.swap(i, min);
				}
			}
		}

	}

	public static void bubbleSort(CompArray array) {
		int n = array.size();
		// TODO: Implement bubbleSort
		for (int i = 0; i <= n-2; i++) {
			for (int j = 0; j <= (n-2-i); j++) {
				if (array.get(j+1).lessThan(array.get(j))) {
					array.swap(j, j+1);
				}
			}
		}

	}

	public static void insertionSort(CompArray array) {
		int n = array.size();

		for (int i = 1; i < n; i += 1) {
			CompElement v = array.get(i);
			int j = i - 1;
			while (j >= 0 && array.get(j).greaterThan(v)) {
				array.put(j + 1, array.get(j));
				j = j - 1;
			}

			array.put(j + 1, v);
		}

	}

	public static void mergeSort(CompArray array) {
		int n = array.size();
		int k = n / 2;
		if (n <= 1) { return; }

		CompArray array1 = new CompArray(k);
		CompArray array2 = new CompArray(n - k);
		// TODO: Copy the first k values of array into array1.
		for (int i = 0; i < k; i += 1) {
			// Fill in this part
			array1.put(i, array.get(i));
		}
		// TODO: copy the remaining n-k values of array into array2.
		for (int i = 0; i < n - k; i += 1) {
			// Fill this part. Note that array[k] is the first entry that needs to go into array2.
			array2.put(i, array.get(k+i));
		}
		mergeSort(array1);
		mergeSort(array2);
		merge(array1, array2, array);
	}

	public static void merge(CompArray source1, CompArray source2, CompArray target) {
		// We are assuming that source1 and source2 are already sorted, and that all three arrays have compatible sizes
		// i.e. size(source1) + size(source2) = size(target)
		// We must here implement the merge of the entries from the two sources into the target
		int p = source1.size();
		int q = source2.size();
		int i = 0;    // Keeps track of index into source1
		int j = 0;    // Keeps track of index into source2
		int k = 0;    // Keeps track of index into target

		while (i < p && j < q) {
			// TODO: Implement the comparison and index increment logic
			if (source1.get(i).lessThanOrEqualTo(source2.get(j))) {
				target.put(k, source1.get(i));
				i += 1;
			}
			else {
				target.put(k, source2.get(j));
				j += 1;
			}
			k += 1;
		}
		if (i == p) {
			// TODO: Copy remaining elements from source2 to target
			while (j < q) {
				target.put(k, source2.get(j));
				k += 1;
				j += 1;
			}
		} else {
			// TODO: Copy remaining elements from source1 to target
			while (i < p) {
				target.put(k, source1.get(i));
				k += 1;
				i += 1;
			}
		}
	}

	public static void quickSort(CompArray array) {
		qsort(array, 0, array.size() - 1);
	}

	// Helper: sorts using quicksort the part of the array from l to r
	private static void qsort(CompArray array, int l, int r) {
		if (l >= r) { return; }

		int s = partition(array, l, r);
		qsort(array, l, s - 1);
		qsort(array, s + 1, r);
	}

	// Implements the Hoare partition scheme
	// Does not make an attempt at picking a good pivot
	private static int partition(CompArray array, int l, int r) {
		CompElement pivot = array.get(l);
		int i = l;
		int j = r + 1;
		do {
			// TODO: Implement this part. Use "do-while" for the repeat's
			do {
				i += 1;
			} while (array.get(i).lessThan(pivot));
			do {
				j -= 1;
			} while (array.get(j).greaterThan(pivot));
			array.swap(i, j);
		} while (i < j);
		// TODO: Some swaps remain before we can return
		array.swap(i, j);
		array.swap(l, j);
		return j;
	}

	public static void heapSort(CompArray array) {
		int size = array.size();
		// We first copy the elements to a larger-by-1 array, so that they start from index 1 (and empty the index 0 entry)
		CompArray workArray = new CompArray(size + 1);
		for (int i = 0; i < size; i += 1) {
			workArray.put(i + 1, array.get(i));
		}
		heapBottomUp(workArray);
		int heapSize = size;  // Marks the location where we'll put the next maximum
		while (heapSize > 0) {
			array.put(heapSize - 1, workArray.get(1));
			workArray.put(1, workArray.get(heapSize));    // Put last element at the heap top. Then we need to fix it.
			heapSize -= 1;
			heapPercolateDown(workArray, 1, heapSize);
		}
	}

	// Implements the algorithm on page 229
	public static void heapBottomUp(CompArray array) {
		// Since the array starts from the empty element zero, the n in the algorithm is not literally the array size
		int n = array.size() - 1;

		for (int i = n; i > 0; i -= 1) {
			heapPercolateDown(array, i, n);
		}
	}

	// Fixes the node at location start by appropriately pushing it down, assuming the lower nodes are already heapified
	// n is the heap size
	public static void heapPercolateDown(CompArray heap, int start, int n) {
		// This method is the body of the for loop in the algorithm on page 229
		// "start" is the "i" in the algorithm
		int j = 0;
		int k = start;
		CompElement v = heap.get(k);
		boolean isHeap = false;
		// TODO : Implement the rest of the algorithm on page 229
		while (isHeap != true && 2*k <= n) {
			j = 2*k;
			if (j < n) {
				if (heap.get(j).lessThan(heap.get(j+1))) {
					j += 1;
				}
			}
			if (v.greaterThanOrEqualTo(heap.get(j))) {
				isHeap = true;
			}
			else {
				heap.put(k, heap.get(j));
				k = j;
			}
			
		}
		heap.put(k, v);

	}

	// Use this method for testing
	public static void main(String[] args) {
		int size = 20;
		int bound = 100;
		// Make new random array
		CompArray array = CompArray.makeRandom(size, bound);
		// Always reset the counts
		CompArray.resetCounts();
		System.out.println("############################################");
		System.out.println("##########    INSERTION SORT     ###########");
		System.out.println("############################################");
		// Print the array before sorting
		System.out.println(array);
		Sort.insertionSort(array);
		// Print the sorted array
		System.out.println(array);
		System.out.format("Element Comparisons: %d\n", array.getNumComparisons());
		System.out.format("Array writes: %d\n", array.getNumWrites());

		// Add your own printouts here
		
		
		// Make new random array
		CompArray array2 = CompArray.makeRandom(size, bound);
		// Always reset the counts
		CompArray.resetCounts();
		System.out.println("############################################");
		System.out.println("##########    Selection Sort     ###########");
		System.out.println("############################################");
		// Print the array before sorting
		System.out.println(array2);
		Sort.selectionSort(array2);
		// Print the sorted array
		System.out.println(array2);
		System.out.format("Element Comparisons: %d\n", array2.getNumComparisons());
		System.out.format("Array writes: %d\n", array2.getNumWrites());
		
		
		// Make new random array
		CompArray array3 = CompArray.makeRandom(size, bound);
		// Always reset the counts
		CompArray.resetCounts();
		System.out.println("############################################");
		System.out.println("##########    Bubble Sort     ###########");
		System.out.println("############################################");
		// Print the array before sorting
		System.out.println(array3);
		Sort.bubbleSort(array3);
		// Print the sorted array
		System.out.println(array3);
		System.out.format("Element Comparisons: %d\n", array3.getNumComparisons());
		System.out.format("Array writes: %d\n", array3.getNumWrites());
		
		
		// Make new random array
		CompArray array4 = CompArray.makeRandom(size, bound);
		// Always reset the counts
		CompArray.resetCounts();
		System.out.println("############################################");
		System.out.println("##########    Merge Sort     ###########");
		System.out.println("############################################");
		// Print the array before sorting
		System.out.println(array4);
		Sort.mergeSort(array4);
		// Print the sorted array
		System.out.println(array4);
		System.out.format("Element Comparisons: %d\n", array4.getNumComparisons());
		System.out.format("Array writes: %d\n", array4.getNumWrites());
		
		
		// Make new random array
		CompArray array5 = CompArray.makeRandom(size, bound);
		// Always reset the counts
		CompArray.resetCounts();
		System.out.println("############################################");
		System.out.println("##########    Quick Sort     ###########");
		System.out.println("############################################");
		// Print the array before sorting
		System.out.println(array5);
		Sort.quickSort(array5);
		// Print the sorted array
		System.out.println(array5);
		System.out.format("Element Comparisons: %d\n", array5.getNumComparisons());
		System.out.format("Array writes: %d\n", array5.getNumWrites());
		
		// Make new random array
		CompArray array6 = CompArray.makeRandom(size, bound);
		// Always reset the counts
		CompArray.resetCounts();
		System.out.println("############################################");
		System.out.println("##########    Heap Sort     ###########");
		System.out.println("############################################");
		// Print the array before sorting
		System.out.println(array6);
		Sort.heapSort(array6);
		// Print the sorted array
		System.out.println(array6);
		System.out.format("Element Comparisons: %d\n", array6.getNumComparisons());
		System.out.format("Array writes: %d\n", array6.getNumWrites());
	}
}
